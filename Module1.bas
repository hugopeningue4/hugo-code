Attribute VB_Name = "Module1"
Sub CreateDropdown()

    ' Declare variables
    Dim ws As Worksheet
    Dim rng As Range

    ' Set the worksheet variable
    Set ws = ThisWorkbook.ActiveSheet

    ' Set the range variable
    Set rng = ws.Range("A2:A" & ws.Cells(ws.Rows.Count, "A").End(xlUp).Row)

    ' Add a dropdown list to the range
    With rng.Validation
        .Delete ' clear any previous validation
        .Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, Operator:= _
        xlBetween, Formula1:= _
        "Termin�,En cours,A arbitrer,Refus�,Supprim�,A lancer,Mise en attente,Gel�,D�part"
        .IgnoreBlank = True
        .InCellDropdown = True
        .InputTitle = ""
        .ErrorTitle = ""
        .InputMessage = ""
        .ErrorMessage = ""
        .ShowInput = True
        .ShowError = True
    End With

    ' Apply the correct color based on current values
    Dim cell As Range
    For Each cell In rng
        ColorRowBasedOnValue cell
    Next cell

End Sub

Sub ColorRowBasedOnValue(ByVal cell As Range)
    Select Case cell.Value
        Case "Termin�"
            cell.EntireRow.Font.ColorIndex = 46
        Case "En cours"
            cell.EntireRow.Font.ColorIndex = 1
        Case "A arbitrer"
            cell.EntireRow.Font.ColorIndex = 3
        Case "Refus�"
            cell.EntireRow.Font.ColorIndex = 23
        Case "Supprim�"
            cell.EntireRow.Font.ColorIndex = 33
        Case "A lancer"
            cell.EntireRow.Font.ColorIndex = 4
        Case "Mise en attente"
            cell.EntireRow.Font.ColorIndex = 1
        Case "Gel�"
            cell.EntireRow.Font.ColorIndex = 41
        Case "D�part"
            cell.EntireRow.Font.ColorIndex = 13.29
        Case Else
            cell.EntireRow.Font.ColorIndex = xlAutomatic 'default font color
    End Select
End Sub




