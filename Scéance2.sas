libname tom "C:\Users\grego\Desktop\SAS M2" ; 

data base1 ; set tom.exemple2 ; 
run ; 

data base_m30 ; set base1 ; 
if age<30 ; 
run ; 

proc freq data=base_m30 ; 
tables sexe ; 
run ;

data base_m30b ; set base_m30 ; 
if sexe="1" then sexeb="1.Homme" ; 
else sexeb="2.Femme" ; 
run ; 

/* sexeb 
sexe="1" > sexeb="1.Homme"
sexe="2" > sexeb="2.Femme" */

data base_p50 ; set base1 ; 
if age>49 ; 
run ;

/* Question 10 */

data base2 ; set base1 ; 
jeunes=(age<30) ; 
run ; 

proc freq data=base2 ; 
tables jeunes ; 
run ; 

data base3 ; set base2 ; 
hommes=(sexe="1") ; 
run ; 

proc freq data=base3 ; 
tables hommes ; 
run ; 

data base4 ; set base3 ; 
seniors=(age>49) ; 
run ; 

proc freq data=base4 ; 
tables seniors ; 
run ; 

data base5 ; set base4 ; 
if age<30 then classe_age="1.Moins de 30 ans" ; 
else if age<50 then classe_age="2.30-49 ans" ; 
else classe_age="3.50 et plus" ; 
run ; 

proc freq data=base5 ; 
tables classe_age ; 
run ; 

/* M2CEES.ees@univ-paris1.fr */

proc format ; 
value age_3mod 
low-29="1.Moins de 30 ans" 
30-49="2.30-49 ans"
50-high="3.50 et plus" ; 
run ; 

proc freq data=base5 ; 
tables age ; 
format age age_3mod. ; 
run ; 

proc format ; 
value $sexe
"1"="1.Homme"
"2"="2.Femme" ; 
run ; 

proc freq data=base5 ; 
tables sexe ;
format sexe $sexe. ; 
run ; 

proc freq data=base5 ; 
tables typemploi/missing ; 
run ; 

data base6 ; set base5 ; 
if typemploi ne "" ; 
run ; 

proc freq data=base6 ; 
tables sexe_hf ; 
weight pondcal ; 
run ; 

data base7 ; set base6 ; 
if age<30 and typemploi="7" then m30cdi="1.Moins de 30 ans en CDI" ; 
else if age<30 then m30cdi="2.Moins de 30 hors CDI" ; 
else if typemploi="7" then m30cdi="3.Plus de 30 en CDI" ; 
else m30cdi="4.Plus de 30 hors CDI" ; 
run ; 

proc freq data=base7 ; 
tables m30cdi ; 
run ; 

data base8 ; set base7 ; 
cdi=(typemploi="7") ; 
run ; 

proc freq data=base8 ; 
tables cdi*jeunes ; 
run ;

data tom.exemple3 ; set base8 ; 
run ; 
